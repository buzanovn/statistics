import math
from scipy.stats import chi2
from scipy.stats import norm
import numpy as np
import argparse

def fill_data(fileName):
    with open(fileName, 'r') as file:
        data = []
        for line in file:
            pair = line.split("\t")
            data.append((int(pair[0]), int(pair[1]), int(pair[2])))
        return data


def calc_sum_values(data):
    sum = 0
    for entry in data:
        sum += entry[1]
    return sum


def calc_mean(data):
    result = 0
    for entry in data:
        result += entry[0] * entry[1]
    return result/calc_sum_values(data)


def calc_cen_mom_N(data, mean, N):
    result = 0
    for entry in data:
        result += (entry[0] - mean) ** N * entry[1]
    return result/calc_sum_values(data)


def calc_asymmetry_mistake(data, asymmetry):
    sum_values = calc_sum_values(data)
    return abs(asymmetry / math.sqrt((6 * sum_values) / (sum_values + 1) * (sum_values + 3)))


def calc_excess_mistake(data, excess):
    sum_values = calc_sum_values(data)
    return abs(excess /  math.sqrt((24 * sum_values * (sum_values - 2) * (sum_values - 3))/((sum_values - 1) ** 2 * (sum_values + 3) * (sum_values + 5))))


def process_variation(variation):
    t = "Выборку считаем "
    o = "однородной"
    n = "неоднородной"
    if variation < (1/3):
        return t + o
    else:
        return t + n


def process_asymmetry(asymmetry, asymmetry_mistake):
    t = "Распределение считаем "
    l = "левосторонне ассиметричным"
    r = "правосторонне ассиметричным"
    s = "симметричным"
    p = ", при том ассиметрия "
    nn = "случайна, незначительна"
    yy = "неслучайна, значима"
    if asymmetry == 0:
        return t + s
    elif asymmetry < 0:
        if asymmetry_mistake > 3:
            return t + l + p + yy
        else:
            return t + l + p + nn
    else:
        if asymmetry_mistake > 3:
            return t + r + p + yy
        else:
            return t + r + p + nn


def process_excess(excess, excess_mistake):
    t = "Распределение считаем "
    p = "плосковершинным"
    o = "островершинным"
    n = "нормальным"
    pp = ", при том эксцесс "
    nn = "случаен, незначителен"
    yy = "неслучаен, значителен"
    if excess == 0:
        return t + n
    elif excess < 0:
        if excess_mistake > 5:
            return t + p + pp + yy
        else:
            return t + p + pp + nn
    else:
        if excess_mistake > 5:
            return t + o + pp + yy
        else:
            return t + o + pp + nn


def reshape_data_for_pearson(data):
    pearson_theor_data = []
    pearson_empir_data = []
    for i in range(0, 10):
        pearson_empir_data.append((i * 10 + 1 if i != 0 else 0, (i+1) * 10, 0))
        pearson_theor_data.append((i * 10 + 1 if i != 0 else 0, (i+1) * 10, 0))
    i = 0
    while i < len(pearson_empir_data):
        for d in data:
            if d[0] >= pearson_empir_data[i][0] and d[0] <= pearson_empir_data[i][1]:
                pearson_empir_data[i] = list(pearson_empir_data[i])
                pearson_empir_data[i][2] += d[1]
                pearson_empir_data[i] = tuple(pearson_empir_data[i])

                pearson_theor_data[i] = list(pearson_theor_data[i])
                pearson_theor_data[i][2] += d[2]
                pearson_theor_data[i] = tuple(pearson_theor_data[i])
        i += 1
    return pearson_empir_data, pearson_theor_data


def calc_pearson_sum(data):
    ped, ptd = reshape_data_for_pearson(data)
    e_sum = 0
    t_sum = 0
    for i in range(0, 10):
        e_sum += ped[i][2]
        t_sum += ptd[i][2]
    prob_theor = []
    for t in ptd:
        prob_theor.append((t[2] * e_sum)/t_sum)
    sum_pearson = 0
    for i in range(0, len(ped)):
        sum_pearson += ((ped[i][2] - prob_theor[i]) ** 2)/prob_theor[i]
    return sum_pearson


def process_pearson_sum(pearson_sum, degrees, theor=True):
    #  Число степеней свободы считаем как 10 - 1
    p = np.array([0.995, 0.99, 0.975, 0.95, 0.9, 0.75, 0.5, 0.25, 0.10, 0.05, 0.025, 0.01, 0.005, 0.001])
    p = p[::-1]
    pp = chi2.isf(p, degrees)
    t = "Отвергаем гипотезу с вероятностью "
    n = "Гипотеза не отвергается, эмпирическое распределение соответствует" + "теоретическому" if theor else "нормальному"
    for i in range(0, len(pp)):
        if pearson_sum > pp[i]:
            return t + str(1-p[i])
    return n


def calc_pearson_sum_nd(data, mean, std_deviation):
    ped = reshape_data_for_pearson(data)[0]
    sum = calc_sum_values(data)
    intervals = []
    norm_borders = []
    fun_distribution = []
    probabilities = []
    theoretical_values = []
    pearson_sum = 0
    for i in range(0, 10):
        intervals.append((i * 10 + 0.5 if i != 0 else 0, (i + 1) * 10 + (0.5 if i != 9 else 0)))
        norm_borders.append((((intervals[i][0] - mean)/std_deviation), ((intervals[i][1] - mean)/std_deviation)))
        fun_distribution.append((norm.cdf(norm_borders[i][0], 0, 1) if i != 0 else 0, norm.cdf(norm_borders[i][1], 0, 1) if i != 9 else 1))
        probabilities.append(fun_distribution[i][1] - fun_distribution[i][0])
        theoretical_values.append(probabilities[i] * sum)
        pearson_sum += (ped[i][2] - theoretical_values[i]) ** 2 / theoretical_values[i]
    return pearson_sum


def calc_file(fileName):
    print("Обсчитываю файл ", fileName)
    data              = fill_data(fileName)
    mean              = calc_mean(data)
    dispersion        = calc_cen_mom_N(data, mean, 2)
    std_deviation     = math.sqrt(dispersion)
    variation         = std_deviation/mean
    cen_mom_3         = calc_cen_mom_N(data, mean, 3)
    cen_mom_4         = calc_cen_mom_N(data, mean, 4)
    asymmetry         = cen_mom_3/(std_deviation ** 3)
    excess            = cen_mom_4/(std_deviation ** 4) - 3
    asymmetry_mistake = calc_asymmetry_mistake(data, asymmetry)
    excess_mistake    = calc_excess_mistake(data, excess)
    pearson_sum       = calc_pearson_sum(data)
    pearson_sum_nd    = calc_pearson_sum_nd(data, mean, std_deviation)

    print("Среднее арифметическое: ", mean)
    print("Дисперсия: ", dispersion)
    print("Среднее арифметическое: ", std_deviation)
    print("Коэффициент вариации: ", variation, process_variation(variation))
    print("Центральный момент третьего порядка: ", cen_mom_3)
    print("Центральный момент четвертого порядка: ", cen_mom_4)
    print("Показатель ассиметрии: ", asymmetry, process_asymmetry(asymmetry, asymmetry_mistake))
    print("Показатель эксцесса: ", excess, process_excess(excess, excess_mistake))
    print("Сумма Пирсона для соответствия эмпирического распределения теоретическому: ", pearson_sum, process_pearson_sum(pearson_sum, 9))
    print("Сумма Пирсона для соответствия эмпирического распределения нормальному: ", pearson_sum_nd, process_pearson_sum(pearson_sum_nd, 7, False))
    print("\n\n")

def process_files(files):
    for file in files:
        try:
            calc_file(file)
        except Exception as e:
            print(e)
            print('Wrong file type.\nIf you see error like \"Blah-blah-blah charactermap blah-blah\" be sure to make this work with \"chcp 65001\" into your console (Windows only)')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--files', nargs='+', help="Pass files here")
    parser.add_argument('-e', '--example', action='store_true', default=False, help="Show example")
    args = parser.parse_args()
    if args.example:
        process_files(['example'])
    elif args.files is not None:
        process_files(args.files)
    else:
        print("No files provided")

if __name__ == "__main__":
    main()
